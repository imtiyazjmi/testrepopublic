<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+lY<>!|FV}UNf^Rl_iCLk^(mzfh=1b#^5.BPS 820a!zNs%&b.hR]}ed5:qV,ICA');
define('SECURE_AUTH_KEY',  '*@t_{PL>p47(g2k)K:@Lt25G9n8fdSNF>D]^)9yG/tvIT>x#ehy2a+sj~2:hU!2s');
define('LOGGED_IN_KEY',    '#Ue IzE`y8t~ol56z89{Ob :0GSKH5[$JMEE`F#V2J^3GlkLoMOTUR.%QL1fe-we');
define('NONCE_KEY',        'j!D2:7T<#T 7R4~(2GcR/DeEi?`ARXs[{02eGx~cXM]3g!Fecksiai 9:tGB]GK>');
define('AUTH_SALT',        'C6Yzps)Zc7l3i|+{0l2Foe|5q0/+AbppWvZP6IQv.5ckN,*oYE*i_#v<2*VfJWRj');
define('SECURE_AUTH_SALT', '6qWd2|T{<Vzt-S~2R3kNE@{Ab`yN>cxL:kW?f//_^ =E0_YG8znCPoYmFGcRL;Qj');
define('LOGGED_IN_SALT',   'x ^6h/&BrJy[wvmPl5d=qT>hvBo[zmL,Y[ .:]D1 TT-%}iscCY3pq& +ih{XtOS');
define('NONCE_SALT',       'x[-scoqWQW<e%`:.:+vM?JZlF&Ol>f7m:aRms#k.A~>dA,/}YH!~1}it&zO&@aM{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
